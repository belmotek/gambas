#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.util 3.18.90\n"
"POT-Creation-Date: 2023-01-19 14:15 UTC\n"
"PO-Revision-Date: 2023-01-19 14:15 UTC\n"
"Last-Translator: gian <gian@gian>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Utility routines"
msgstr "Routine di utilità"

# gb-ignore
#: File.class:22
msgid "&1 B"
msgstr ""

# gb-ignore
#: File.class:24
msgid "&1 KiB"
msgstr ""

# gb-ignore
#: File.class:26
msgid "&1 MiB"
msgstr ""

# gb-ignore
#: File.class:28
msgid "&1 GiB"
msgstr ""

# gb-ignore
#: File.class:34
msgid "&1 KB"
msgstr ""

# gb-ignore
#: File.class:36
msgid "&1 MB"
msgstr ""

# gb-ignore
#: File.class:38
msgid "&1 GB"
msgstr ""

#: Language.class:35
msgid "Afrikaans (South Africa)"
msgstr "Afrikaans (Sudafrica)"

#: Language.class:39
msgid "Arabic (Egypt)"
msgstr "Arabo (Egitto)"

#: Language.class:40
msgid "Arabic (Tunisia)"
msgstr "Arabo (Tunisia)"

#: Language.class:41
msgid "Arabic (Algeria)"
msgstr "Arabo (Algeria)"

#: Language.class:46
msgid "Azerbaijani (Azerbaijan)"
msgstr "Azerbaigiano (Azerbaijan)"

#: Language.class:50
msgid "Bulgarian (Bulgaria)"
msgstr "Bulgaro (Bulgaria)"

#: Language.class:54
msgid "Catalan (Catalonia, Spain)"
msgstr "Catalano (Catalano, Spagna)"

#: Language.class:60
msgid "Welsh (United Kingdom)"
msgstr "Gallese (UK)"

#: Language.class:64
msgid "Czech (Czech Republic)"
msgstr "Ceco (Repubblica Ceca)"

#: Language.class:69
msgid "Danish (Denmark)"
msgstr "Danese (Danimarca)"

#: Language.class:73
msgid "German (Germany)"
msgstr "Tedesco (Germania)"

#: Language.class:74
msgid "German (Belgium)"
msgstr "Tedesco (Belgio)"

#: Language.class:79
msgid "Greek (Greece)"
msgstr "Greco (Grecia)"

#: Language.class:83
msgid "English (common)"
msgstr "Inglese (Internazionale)"

#: Language.class:84
msgid "English (United Kingdom)"
msgstr "Inglese (Regno Unito)"

#: Language.class:85
msgid "English (U.S.A.)"
msgstr "Inglese (U.S.A.)"

#: Language.class:86
msgid "English (Australia)"
msgstr "Inglese (Australia)"

#: Language.class:87
msgid "English (Canada)"
msgstr "Inglese (Canada)"

#: Language.class:88
msgid "English (India)"
msgstr "Inglese (India)"

#: Language.class:93
msgid "Esperanto (Anywhere!)"
msgstr "Esperanto (Ovunque!)"

#: Language.class:97
msgid "Spanish (common)"
msgstr "Spagnolo (comune)"

#: Language.class:98
msgid "Spanish (Spain)"
msgstr "Spagnolo (Spagna)"

#: Language.class:99
msgid "Spanish (Argentina)"
msgstr "Spagnolo (Argentina)"

#: Language.class:104
msgid "Estonian (Estonia)"
msgstr "Estone (Estonia)"

#: Language.class:108
msgid "Basque (Basque country)"
msgstr "Basco (Regione Basca)"

#: Language.class:112
msgid "Farsi (Iran)"
msgstr "Farsi (Iran)"

#: Language.class:116
msgid "Finnish (Finland)"
msgstr "Finnico (Finlandia)"

#: Language.class:121
msgid "French (France)"
msgstr "Francese (Francia)"

#: Language.class:122
msgid "French (Belgium)"
msgstr "Francese (Belgio)"

#: Language.class:123
msgid "French (Canada)"
msgstr "Francese (Canada)"

#: Language.class:124
msgid "French (Switzerland)"
msgstr "Francese (Svizzera)"

#: Language.class:129
msgid "Galician (Spain)"
msgstr "Galiziano (Spagna)"

#: Language.class:133
msgid "Hebrew (Israel)"
msgstr "Ebraico (Israele)"

#: Language.class:137
msgid "Hindi (India)"
msgstr "Hindi (India)"

#: Language.class:141
msgid "Hungarian (Hungary)"
msgstr "Ungherese (Ungheria)"

#: Language.class:145
msgid "Croatian (Croatia)"
msgstr "Croato (Croazia)"

#: Language.class:149
msgid "Indonesian (Indonesia)"
msgstr "Indonesiano (Indonesia)"

#: Language.class:154
msgid "Tamil (India)"
msgstr "Tamil (India)"

#: Language.class:159
msgid "Telugu (India)"
msgstr "Telugu (India)"

#: Language.class:164
msgid "Malayalam (India)"
msgstr "Malayalam (India)"

#: Language.class:169
msgid "Kannada (India)"
msgstr "Kannada (India)"

#: Language.class:179
msgid "Punjabi (India)"
msgstr "Punjabi (India)"

#: Language.class:199
msgid "Irish (Ireland)"
msgstr "Irlandese (Irlanda)"

#: Language.class:203
msgid "Icelandic (Iceland)"
msgstr "Islandese (Islanda)"

#: Language.class:207
msgid "Italian (Italy)"
msgstr "Italiano (Italia)"

#: Language.class:208
msgid "Italian (Switzerland)"
msgstr "Italiano (Svizzera)"

#: Language.class:213
msgid "Japanese (Japan)"
msgstr "Giapponese (Giappone)"

#: Language.class:217
msgid "Khmer (Cambodia)"
msgstr "Khmer (Cambogia)"

#: Language.class:221
msgid "Korean (Korea)"
msgstr "Coreano (Corea)"

#: Language.class:226
msgid "Latin"
msgstr "Latino"

#: Language.class:230
msgid "Lithuanian (Lithuania)"
msgstr "Lituano (Lituania)"

#: Language.class:234
msgid "Macedonian (Republic of Macedonia)"
msgstr "Macedone (Repubblica di Macedonia)"

#: Language.class:238
msgid "Dutch (Netherlands)"
msgstr "Danese (Olanda)"

#: Language.class:239
msgid "Dutch (Belgium)"
msgstr "Danese (Belgio)"

#: Language.class:244
msgid "Norwegian (Norway)"
msgstr "Norvegese (Norvegia)"

#: Language.class:245
msgid "Bokmål (Norway)"
msgstr "Bokmål (Norvegia)"

#: Language.class:250
msgid "Polish (Poland)"
msgstr "Polacco (Polonia)"

#: Language.class:254
msgid "Portuguese (Portugal)"
msgstr "Portoghese (Portogallo)"

#: Language.class:255
msgid "Portuguese (Brazil)"
msgstr "Portoghese (Brasile)"

#: Language.class:259
msgid "Valencian (Valencian Community, Spain)"
msgstr "Valenzano (Comunità Valenzana, Spagna)"

#: Language.class:263
msgid "Romanian (Romania)"
msgstr "Rumeno (Romania)"

#: Language.class:267
msgid "Russian (Russia)"
msgstr "Russo (Russia)"

#: Language.class:272
msgid "Slovenian (Slovenia)"
msgstr "Slovacco (Slovenia)"

#: Language.class:276
msgid "Albanian (Albania)"
msgstr "Albanese (Albania)"

#: Language.class:280
msgid "Serbian (Serbia & Montenegro)"
msgstr "Serbo (Serbia e Montenegro)"

#: Language.class:284
msgid "Swedish (Sweden)"
msgstr "Svedese (Svezia)"

#: Language.class:288
msgid "Turkish (Turkey)"
msgstr "Turco (Turchia)"

#: Language.class:293
msgid "Ukrainian (Ukrain)"
msgstr "Ucraino (Ucraina)"

#: Language.class:297
msgid "Vietnamese (Vietnam)"
msgstr "Vietnamita (Vietnam)"

#: Language.class:301
msgid "Wallon (Belgium)"
msgstr "Vallone (Belgio)"

#: Language.class:305
msgid "Simplified chinese (China)"
msgstr "Cinese semplificato (China)"

#: Language.class:306
msgid "Traditional chinese (Taiwan)"
msgstr "Cinese tradizionale (Taiwan)"

#: Language.class:383
msgid "Unknown"
msgstr "Sconosciuto"
